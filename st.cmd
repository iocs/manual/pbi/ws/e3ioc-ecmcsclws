epicsEnvSet IOC PBI-WS06:Ctrl-ECAT-100  # HEBT
epicsEnvSet IOC PBI-WS05:Ctrl-ECAT-100  # A2T
epicsEnvSet IOC PBI-WS04:Ctrl-ECAT-100  # HBL
epicsEnvSet IOC PBI-WS03:Ctrl-ECAT-100  # MBL 1
epicsEnvSet IOC PBI-WS03:Ctrl-ECAT-200  # MBL 2
epicsEnvSet IOC PBI-WS03:Ctrl-ECAT-300  # MBL 3
epicsEnvSet IOC PBI-WS02:Ctrl-ECAT-100  # SPK 1
epicsEnvSet IOC PBI-WS02:Ctrl-ECAT-200  # SPK 2
epicsEnvSet IOC PBI-WS02:Ctrl-ECAT-300  # SPK 3
epicsEnvSet IOC SCLWS

require essioc
require ecmccfg 8.0.0

iocshLoad "${ecmccfg_DIR}/startup.cmd" "ECMC_VER=8.0.2, NAMING=ESSnaming"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EK1100
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL1808
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL2819

# Set all outputs to feed switches
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput01,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput02,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput03,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput05,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput06,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput07,1)"

# Both EL7201 are being used only as resolver readers!
# Motor control is done by EL7047
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7201
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7201

# The Mclennan 23HSX206 + EL7047 combo is not available in ecmccfg.
# Use addSlave and manually configure it below.
# If this ever gets added to ecmccfg switch back to configureSlave.

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7047

# Set speed range to 8000 full steps per second
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x5,3,1)"
# Activate "enable hardware" function
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x32,1,1)"
# Use EL7047 brake controll instead of ecmc's
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x3A,1,1)"

# Max current: 4.2 A
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,4200,2)"
# Reduced current: 500 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,500,2)"
# Voltage: 48 V
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x3,4800,2)"
# Coil resistance: 0.35 Ohms
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x4,35,2)"
# Motor full steps: 200
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x6,200,2)"
# Coil inductance: 0.45 mH
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0xA,45,2)"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL7047

# Set speed range to 8000 full steps per second
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x5,3,1)"
# Activate "enable hardware" function
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x32,1,1)"
# Use EL7047 brake controll instead of ecmc's
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x3A,1,1)"

# Max current: 4.2 A
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,4200,2)"
# Reduced current: 500 mA
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,500,2)"
# Voltage: 48 V
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x3,4800,2)"
# Coil resistance: 0.35 Ohms
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x4,35,2)"
# Motor full steps: 200
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x6,200,2)"
# Coil inductance: 0.45 mH
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0xA,45,2)"

iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis1.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis2.ax"

# Set home switches to normally open
ecmcConfigOrDie "Cfg.SetAxisMonHomeSwitchPolarity(1, 1)"
ecmcConfigOrDie "Cfg.SetAxisMonHomeSwitchPolarity(2, 1)"

iocshLoad "${essioc_DIR}/common_config.iocsh"

ecmcConfigOrDie "Cfg.EcApplyConfig(1)"
iocshLoad "${ecmccfg_DIR}/setAppMode.cmd"
