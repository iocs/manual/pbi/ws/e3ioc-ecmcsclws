#!/bin/env python3
"""Constantly move to random positions"""

from argparse import ArgumentParser
from random import uniform
from time import monotonic
from epics import PV

def main(args):

	red_text = "\33[31m"
	green_text = "\33[32m"
	yellow_text = "\33[33m"
	reset = "\33[m"

	print(f"{red_text}Before continuing make sure the limit switches work, the axis is homed and it is safe to move!!!{reset}")
	try: input("Press ENTER to start or press Ctrl+c to exit now")
	except KeyboardInterrupt: return
	print()

	error = False
	def exit_on_error(*args, **kwargs):
		nonlocal error
		if "conn" in kwargs:
			error = error or not kwargs["conn"]
		elif "value" in kwargs:
			error = error or kwargs["value"]

	print("Connecting to PV")
	val = PV(args.pv)
	errid = PV(args.pv + "-ErrId", callback=exit_on_error, connection_callback=exit_on_error)
	assert val.wait_for_connection()
	assert errid.wait_for_connection()

	print("Starting random motion test")
	print(f"Range: from {args.lower} to {args.upper}")
	print(f"Duration: {args.time} hour(s)\n")

	start_time = monotonic()
	while True:
		setpoint = uniform(args.lower, args.upper)
		print(f"Moving to: {setpoint}")
		val.put(setpoint, True)
		now = monotonic()
		if error or (now - start_time > 3600 * args.time): break

	print()
	if error: print(f"{yellow_text}Stopping on error, total duration: {(now - start_time)/60} minutes{reset}")
	else: print(f"{green_text}Test completed with no errors{reset}")

if __name__ == "__main__":
	parser = ArgumentParser(description=__doc__)
	parser.add_argument("pv", help="ecmc motor record PV name (requires ESS naming)")
	parser.add_argument("-l", "--lower", default=0, help="motion lower limit", type=float)
	parser.add_argument("-u", "--upper", default=1, help="motion upper limit", type=float)
	parser.add_argument("-t", "--time", default=1, help="test duration in hours", type=float)
	main(parser.parse_args())
